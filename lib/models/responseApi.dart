class ResponseApi{
  final int codeResponse;
  final String contentResponse;

  ResponseApi({
    this.codeResponse,
    this.contentResponse
  });
}