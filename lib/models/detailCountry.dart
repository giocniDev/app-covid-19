import 'package:intl/intl.dart';

class DetailCountry{
  final int Confirmed;
  final int Deaths;
  final int Recovered;
  final String Date;

  DetailCountry({
    this.Confirmed,
    this.Date,
    this.Deaths,
    this.Recovered
  });

  String getFormatedValue(int value){
    final formatter = new NumberFormat("#,###");
    return formatter.format(value);
  }

}