import 'package:intl/intl.dart';

class Country{
  final String Name;
  final String Slug;
  final String ISO2;
  final String Code;
  final int NewConfirmed;
  final int TotalConfirmed;
  final int NewDeaths;
  final int TotalDeaths;
  final int NewRecovered;
  final int TotalRecovered;

  Country({
    this.Name,
    this.ISO2,
    this.Slug,
    this.Code,
    this.NewConfirmed,
    this.NewDeaths,
    this.NewRecovered,
    this.TotalConfirmed,
    this.TotalDeaths,
    this.TotalRecovered
  });

  String getFormatedValue(int value){
    final formatter = new NumberFormat("#,###");
    return formatter.format(value);
  }
}