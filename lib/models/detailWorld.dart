import 'package:intl/intl.dart';

class DetailWorld{
  final int TotalConfirmed;
  final int TotalDeaths;
  final int TotalRecovered;

  DetailWorld({
    this.TotalConfirmed,
    this.TotalRecovered,
    this.TotalDeaths
  });

  String getFormatedValue(int value){
    final formatter = new NumberFormat("#,###");
    return formatter.format(value);
  }

}