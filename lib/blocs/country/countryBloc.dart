import 'package:appcovid19/models/country.dart';
import 'package:appcovid19/models/detailCountry.dart';
import 'package:appcovid19/repositories/countryRepository.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'countryEvent.dart';
import 'countryState.dart';
export 'countryEvent.dart';
export 'countryState.dart';

class CountryBloc extends Bloc<CountryEvent,CountryState>{

  List<Country> _listCountryStatus = List();
  CountryRepository _countryRepository;
  bool _order = false;

  CountryBloc({
    @required CountryRepository countryRepository
  }): assert(countryRepository != null){
    _countryRepository = countryRepository;
  }

  @override
  CountryState get initialState =>CountryInitState();

  @override
  Stream<CountryState> mapEventToState(CountryEvent event) async*{
    if(event is CountryGetListEvent){
      yield* _mapGetListToState(event);
    }else if(event is CountryAlternateOrderListEvent){
      yield* _mapOrderListToState();
    }else if(event is CountryFilterListEvent){
      yield* _mapFilterListToState(event);
    }else if(event is CountryGetDetailEvent){
      yield* _mapGetDetailToState(event);
    }
  }

  Stream<CountryState> _mapGetDetailToState(CountryGetDetailEvent event) async*{
    yield CountryLoadingState();
    List<DetailCountry> result =  await _countryRepository.getDetailCountry(event.slug);
    print("TAM: ${result.length}");
    yield CountryDetailState(
      list: result
    );

    //LAST REPORT
    yield CountrySingleDetailState(
      detail: result.last
    );
  }

  Stream<CountryState> _mapFilterListToState(CountryFilterListEvent event) async*{
    List<Country> temp = _listCountryStatus.where((item){
      if(item.Name.toLowerCase().contains(event.value.toLowerCase()) || item.Code.toLowerCase().contains(event.value.toLowerCase())) return true;
      return false;
    }).toList();
    yield CountryListState(
      list: temp,
      cleanFilter: false,
    );
  }

  Stream<CountryState> _mapOrderListToState() async*{
    _order = !_order;
    _listCountryStatus.sort((c1,c2){
      if(_order) return c2.TotalConfirmed.compareTo(c1.TotalConfirmed);
      return c1.TotalConfirmed.compareTo(c2.TotalConfirmed);
    });
    yield CountryListState(
      list: _listCountryStatus,
      cleanFilter: true,
    );
  }

  Stream<CountryState> _mapGetListToState(CountryGetListEvent event) async*{
    yield CountryLoadingState();
    _order = false;
    _listCountryStatus = await _countryRepository.getListWithStatus();
    add(CountryAlternateOrderListEvent());
  }

}