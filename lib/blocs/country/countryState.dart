import 'package:appcovid19/models/country.dart';
import 'package:appcovid19/models/detailCountry.dart';

abstract class CountryState{}

class CountryInitState extends CountryState{}

class CountryLoadingState extends CountryState{}

class CountryErrorState extends CountryState{
  final String message;

  CountryErrorState({
    this.message
  });
}

class CountryListState extends CountryState{
  final List<Country> list;
  final bool cleanFilter;

  CountryListState({
    this.list,
    this.cleanFilter
  });
}

class CountryDetailState extends CountryState{
  final List<DetailCountry> list;

  CountryDetailState({
    this.list
  });
}

class CountrySingleDetailState extends CountryState{
  final DetailCountry detail;

  CountrySingleDetailState({
    this.detail
  });
}