abstract class CountryEvent{}

class CountryGetListEvent extends CountryEvent{}

class CountryGetDetailEvent extends CountryEvent{
  final String slug;

  CountryGetDetailEvent({
    this.slug
  });
}

class CountryAlternateOrderListEvent extends CountryEvent{}

class CountryFilterListEvent extends CountryEvent{
  final String value;

  CountryFilterListEvent({
    this.value
  });
}