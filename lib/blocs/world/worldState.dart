import 'package:appcovid19/models/detailWorld.dart';

abstract class WorldState{}

class WorldInitState extends WorldState{}

class WorldLoadingState extends WorldState{}

class WorldErrorState extends WorldState{
  final String message;
  WorldErrorState({
    this.message
  });
}

class WorldDetailState extends WorldState{
  final DetailWorld detial;

  WorldDetailState({
    this.detial
  });
}