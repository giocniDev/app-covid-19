import 'package:appcovid19/models/detailWorld.dart';
import 'package:appcovid19/repositories/worldRepository.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'worldEvent.dart';
import 'worldState.dart';
export 'worldEvent.dart';
export 'worldState.dart';

class WorldBloc extends Bloc<WorldEvent,WorldState>{

  WorldRepository _worldRepository;

  WorldBloc({
    @required WorldRepository worldRepository
  }): assert(worldRepository != null){
    _worldRepository = worldRepository;
  }

  @override
  WorldState get initialState => WorldInitState();

  @override
  Stream<WorldState> mapEventToState(WorldEvent event) async*{
    if(event is WorldGetDetailEvent){
      yield* _mapGetDetailToState();
    }
  }

  Stream<WorldState> _mapGetDetailToState() async*{
    yield WorldLoadingState();
    DetailWorld detail = await _worldRepository.getDetail();
    yield WorldDetailState(
      detial: detail
    );
  }

}