import 'package:appcovid19/blocs/country/countryBloc.dart';
import 'package:appcovid19/blocs/world/worldBloc.dart';
import 'package:appcovid19/repositories/countryRepository.dart';
import 'package:appcovid19/repositories/worldRepository.dart';
import 'package:appcovid19/views/homeView.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'App covid-19',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MultiBlocProvider(
        providers: [
          BlocProvider<CountryBloc>(
            create: (BuildContext context) => CountryBloc(
              countryRepository: CountryRepository()
            ),
          ),
          BlocProvider<WorldBloc>(
            create: (BuildContext context) => WorldBloc(
                worldRepository: WorldRepository()
            ),
          ),
        ],
        child: HomeView(),
      ),
    );
  }
}

