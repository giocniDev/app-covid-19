import 'package:appcovid19/blocs/country/countryBloc.dart';
import 'package:appcovid19/models/country.dart';
import 'package:appcovid19/models/detailCountry.dart';
import 'package:appcovid19/repositories/countryRepository.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class DetailCountryView extends StatefulWidget {

  final Country country;

  DetailCountryView({
    Key key,
    this.country
  }) : super(key: key);

  @override
  _DetailCountryViewState createState() => _DetailCountryViewState();
}

class _DetailCountryViewState extends State<DetailCountryView> {

  CountryBloc _countryBloc;

  @override
  void initState(){
    super.initState();
    _countryBloc = CountryBloc(
      countryRepository: CountryRepository()
    );
    _countryBloc.add(CountryGetDetailEvent(
      slug: widget.country.Slug
    ));
  }

  @override
  void dispose(){
    _countryBloc.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.country.Name),
      ),
      body: Padding(
        padding: EdgeInsets.all(16),
        child: Column(
          children: <Widget>[
            BlocBuilder<CountryBloc,CountryState>(
              condition: (prev,state){
                if(state is CountryLoadingState || state is CountrySingleDetailState) return true;
                return false;
              },
              bloc: _countryBloc,
              builder: (ctx,state){
                return Table(
                  border: TableBorder.all(
                      color: Colors.grey,
                      width: 0.5
                  ),
                  children: [
                    TableRow(
                        children: [
                          TableCell(
                            child: Padding(
                              padding: EdgeInsets.all(6),
                              child: Text("Confirmed:"),
                            ),
                          ),
                          TableCell(
                            child: Padding(
                              padding: EdgeInsets.all(6),
                              child: Center(
                                child: Text((state is CountrySingleDetailState) ? state.detail.getFormatedValue(state.detail.Confirmed) : "--"),
                              ),
                            ),
                          ),
                        ]
                    ),
                    TableRow(
                        children: [
                          TableCell(
                            child: Padding(
                              padding: EdgeInsets.all(6),
                              child: Text("Deaths:"),
                            ),
                          ),
                          TableCell(
                            child: Padding(
                              padding: EdgeInsets.all(6),
                              child: Center(
                                child: Text((state is CountrySingleDetailState) ? state.detail.getFormatedValue(state.detail.Deaths) : "--"),
                              ),
                            ),
                          ),
                        ]
                    ),
                    TableRow(
                        children: [
                          TableCell(
                            child: Padding(
                              padding: EdgeInsets.all(6),
                              child: Text("Recovered:"),
                            ),
                          ),
                          TableCell(
                            child: Padding(
                              padding: EdgeInsets.all(6),
                              child: Center(
                                child: Text((state is CountrySingleDetailState) ? state.detail.getFormatedValue(state.detail.Recovered) : "--"),
                              ),
                            ),
                          ),
                        ]
                    ),
                  ],
                );
              },
            ),
            Divider(
              height: 50,
            ),
            Text("Historical",
              style: TextStyle(
                fontSize: 20
              ),
            ),
            SizedBox(
              height: 20,
            ),
            Expanded(
              child: BlocBuilder<CountryBloc,CountryState>(
                condition: (prev,state){
                  if(state is CountryLoadingState || state is CountryDetailState) return true;
                  return false;
                },
                bloc: _countryBloc,
                builder: (ctx,state){
                  if(state is CountryDetailState){
                    if(state.list.length == 0) return Center(
                      child: Text("No records"),
                    );
                    return ListView.separated(
                      itemCount: state.list.length,
                      separatorBuilder: (BuildContext ctx,int index){
                        return Divider(
                          height: 30,
                        );
                      },
                      itemBuilder: (BuildContext ctx,int index){
                        return Column(
                          children: <Widget>[
                            Text(state.list[index].Date.split("T")[0],
                              style: TextStyle(
                                fontSize: 16
                              ),
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            Row(
                              mainAxisSize: MainAxisSize.max,
                              children: <Widget>[
                                Expanded(
                                  flex: 1,
                                  child: Column(
                                    mainAxisSize: MainAxisSize.min,
                                    children: <Widget>[
                                      Text("Confirmed"),
                                      Text(state.list[index].getFormatedValue(state.list[index].Confirmed))
                                    ],
                                  ),
                                ),
                                Expanded(
                                  flex: 1,
                                  child: Column(
                                    mainAxisSize: MainAxisSize.min,
                                    children: <Widget>[
                                      Text("Deaths"),
                                      Text(state.list[index].getFormatedValue(state.list[index].Deaths))
                                    ],
                                  ),
                                ),
                                Expanded(
                                  flex: 1,
                                  child: Column(
                                    mainAxisSize: MainAxisSize.min,
                                    children: <Widget>[
                                      Text("Recovered"),
                                      Text(state.list[index].getFormatedValue(state.list[index].Recovered))
                                    ],
                                  ),
                                ),
                              ],
                            )
                          ],
                        );
                      },
                    );
                  }
                  if(state is CountryLoadingState) return Center(
                    child: CircularProgressIndicator(
                      strokeWidth: 1.6,
                    )
                  );
                  return Container();
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}