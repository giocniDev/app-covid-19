import 'package:appcovid19/blocs/world/worldBloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class WorldView extends StatefulWidget {

  WorldView({
    Key key,
  }) : super(key: key);

  @override
  _WorldViewState createState() => _WorldViewState();
}

class _WorldViewState extends State<WorldView> {

  WorldBloc _worldBloc;

  @override
  void initState(){
    super.initState();
    _worldBloc = BlocProvider.of<WorldBloc>(context);
    _worldBloc.add(WorldGetDetailEvent());
  }

  @override
  void dispose(){
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(16),
      child: Column(
        children: <Widget>[
          Center(
            child: Image.asset("assets/covid_world.png",
              height: 200,
              width: 200,
            ),
          ),
          SizedBox(
            height: 30,
          ),
          BlocBuilder<WorldBloc,WorldState>(
            bloc: _worldBloc,
            builder: (ctx,state){
              return Column(
                children: <Widget>[
                  Text("Confirmed",
                    style: TextStyle(
                        fontSize: 18
                    ),
                  ),
                  Text((state is WorldDetailState) ? state.detial.getFormatedValue(state.detial.TotalConfirmed) : "--"),
                  SizedBox(
                    height: 30,
                  ),
                  Row(
                    mainAxisSize: MainAxisSize.max,
                    children: <Widget>[
                      Expanded(
                        flex: 1,
                        child: Column(
                          children: <Widget>[
                            Text("Death",
                              style: TextStyle(
                                  fontSize: 18
                              ),
                            ),
                            Text((state is WorldDetailState) ? state.detial.getFormatedValue(state.detial.TotalDeaths) : "--"),
                          ],
                        ),
                      ),
                      Expanded(
                        flex: 1,
                        child: Column(
                          children: <Widget>[
                            Text("Recovered",
                              style: TextStyle(
                                  fontSize: 18
                              ),
                            ),
                            Text((state is WorldDetailState) ? state.detial.getFormatedValue(state.detial.TotalRecovered) : "--"),
                          ],
                        ),
                      ),
                    ],
                  ),
                ],
              );
            },
          ),
          SizedBox(
            height: 20,
          ),
          BlocBuilder<WorldBloc,WorldState>(
            bloc: _worldBloc,
            builder: (ctx,state){
              if(state is WorldLoadingState) return CircularProgressIndicator(
                strokeWidth: 1.6,
              );
              return Container();
            },
          ),
        ],
      ),
    );
  }
}