import 'package:appcovid19/blocs/country/countryBloc.dart';
import 'package:appcovid19/models/country.dart';
import 'package:appcovid19/views/detailCountryView.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class ListCountriesView extends StatefulWidget {

  ListCountriesView({
    Key key,
  }) : super(key: key);

  @override
  _ListCountriesViewState createState() => _ListCountriesViewState();
}

class _ListCountriesViewState extends State<ListCountriesView> {

  CountryBloc _countryBloc;
  TextEditingController _txtFilter = TextEditingController();

  @override
  void initState(){
    super.initState();
    _countryBloc = BlocProvider.of<CountryBloc>(context);
    _countryBloc.add(CountryGetListEvent());
  }

  @override
  void dispose(){
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<CountryBloc,CountryState>(
      listener: (ctx,state){
        if(state is CountryListState && state.cleanFilter){
          _txtFilter.text = "";
        }
      },
      child: BlocBuilder<CountryBloc,CountryState>(
        bloc: _countryBloc,
        builder: (ctx,state){
          if(state is CountryListState) {
            return Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Container(
                  margin: EdgeInsets.only(top: 5,bottom: 5),
                  child: TextField(
                    controller: _txtFilter,
                    onChanged: (String value){
                      _countryBloc.add(CountryFilterListEvent(value: value));
                    },
                    decoration: InputDecoration(
                        prefixIcon: Icon(Icons.search),
                        contentPadding: EdgeInsets.all(16),
                        hintText: "Filter"
                    ),
                  ),
                ),
                Expanded(
                  child: RefreshIndicator(
                    onRefresh: ()async{
                      _countryBloc.add(CountryGetListEvent());
                    },
                    child: ListView(
                      addAutomaticKeepAlives: true,
                      children: state.list.map((Country country){
                        return ListTile(
                          onTap: (){
                            Navigator.push(
                              context,
                              MaterialPageRoute(builder: (context) => DetailCountryView(
                                country: country,
                              )),
                            );
                          },
                          title: Text(country.Name),
                          subtitle: Text(country.Code),
                          //leading: Icon(Icons.flag),
                          trailing: Text(country.getFormatedValue(country.TotalConfirmed)),
                        );
                      }).toList(),
                    ),
                  ),
                )
              ],
            );
          }
          if(state is CountryLoadingState) {
            return Center(
              child: CircularProgressIndicator(
                strokeWidth: 1.6,
              ),
            );
          }
          if(state is CountryErrorState) {
            return Center(
              child: Text(state.message),
            );
          }
          return Container();
        },
      ),
    );
  }
}
