import 'package:appcovid19/blocs/bottomNavigation/bottomNavigationBloc.dart';
import 'package:appcovid19/blocs/country/countryBloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'listCountriesView.dart';
import 'worldView.dart';

class HomeView extends StatefulWidget {

  HomeView({
    Key key, 
  }) : super(key: key);

  @override
  _HomeViewState createState() => _HomeViewState();
}

class _HomeViewState extends State<HomeView> {

  BottomNavigationBloc _bottomNavigationBloc;
  CountryBloc _countryBloc;

  @override
  void initState(){
    super.initState();
    _bottomNavigationBloc = BottomNavigationBloc();
    _countryBloc = BlocProvider.of<CountryBloc>(context);
  }

  @override
  void dispose(){
    _bottomNavigationBloc.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: BlocBuilder<BottomNavigationBloc,int>(
          bloc: _bottomNavigationBloc,
          builder: (ctx,state){
            switch(state){
              case 0:
                return Text("Confirmed cases");
              case 1:
                return Text("World statistics");
              default:
                return Text("--");
            }
          },
        ),
        actions: <Widget>[
          BlocBuilder<BottomNavigationBloc,int>(
            bloc: _bottomNavigationBloc,
            builder: (ctx,state){
              switch(state){
                case 0:
                  return IconButton(
                    icon: Icon(Icons.sort),
                    onPressed: (){
                      _countryBloc.add(CountryAlternateOrderListEvent());
                    },
                  );
                case 1:
                  return Container();
                default:
                  return Container();
              }
            },
          ),
        ],
      ),
      body: BlocBuilder<BottomNavigationBloc,int>(
        bloc: _bottomNavigationBloc,
        builder: (ctx,state){
          switch(state){
            case 0:
              return ListCountriesView();
            case 1:
              return WorldView();
            default:
              return Container();
          }
        },
      ),
      bottomNavigationBar: BlocBuilder<BottomNavigationBloc,int>(
        bloc: _bottomNavigationBloc,
        builder: (ctx,state){
          return BottomNavigationBar(
            currentIndex: state,
            onTap: (int index){
              _bottomNavigationBloc.add(index);
            },
            items: [
              BottomNavigationBarItem(
                icon: new Icon(Icons.flag),
                title: new Text('Countries'),
              ),
              BottomNavigationBarItem(
                icon: new Icon(Icons.vpn_lock),
                title: new Text('World'),
              ),
            ],
          );
        },
      ),
    );
  }
}