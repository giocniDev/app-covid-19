import 'package:appcovid19/config/configApp.dart';
import 'package:appcovid19/models/detailWorld.dart';
import 'package:appcovid19/models/responseApi.dart';
import 'dart:convert' as convert;
import 'apiHttp.dart';

class WorldRepository{

  Future<DetailWorld> getDetail()async{
    DetailWorld result = null;
    ApiHttp apiHttp = ApiHttp(endPoint: ConfigApp.getEndPointCovid19());
    ResponseApi response = await apiHttp.api(ApiMethod.GET, "/world/total");
    if(response.codeResponse == 200){
      var bodyRequest = convert.jsonDecode(response.contentResponse);
      result = DetailWorld(
          TotalRecovered: int.parse(bodyRequest['TotalRecovered'].toString()),
          TotalDeaths: int.parse(bodyRequest['TotalDeaths'].toString()),
          TotalConfirmed: int.parse(bodyRequest['TotalConfirmed'].toString())
      );
    }
    return result;
  }

}