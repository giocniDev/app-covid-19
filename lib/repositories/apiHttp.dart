import 'package:appcovid19/models/responseApi.dart';
import 'package:http/http.dart' as http;

enum ApiMethod{
  GET,
  POST,
  PUT,
  DELETE
}

class ApiHttp{

  String TAG = "ApiHttp";
  String _baseUrl;
  int CodeResponse;

  ApiHttp({
    String endPoint
  }){
    this._baseUrl = endPoint;
  }

  String _setParametros(Map<String,dynamic> parametros){
    String result = "?";
    parametros.forEach((key,value){
      result += key.toString();
      result += "=";
      result += value.toString();
      result += "&";
    });
    return result.substring(0, result.length - 1);
  }

  Map<String,String> _setHeaders(){
    Map<String,String> headers = {
      "Content-Type" : "application/json"
    };
    return headers;
  }

  Future<ResponseApi> api(ApiMethod method,String url, {Map<String,dynamic> params,Map<String,dynamic> body}) async{
    String strParams = "";
    if(params != null){
      strParams = _setParametros(params);
    }
    String urlFull = this._baseUrl + url + strParams;
    print("FULL URL $urlFull");
    var response;
    switch(method){
      case ApiMethod.GET:
        response = await http.get(urlFull, headers: _setHeaders());
        break;
      case ApiMethod.POST:
        response = await http.post(urlFull,body: body, headers: _setHeaders());
        break;
      case ApiMethod.PUT:
        response = await http.put(urlFull,body: body, headers: _setHeaders());
        break;
      case ApiMethod.DELETE:
        response = await http.delete(urlFull, headers: _setHeaders());
        break;
    }
    //convert.jsonDecode(response.body)
    return ResponseApi(
      codeResponse: response.statusCode,
      contentResponse: response.body
    );
  }

}