import 'package:appcovid19/config/configApp.dart';
import 'package:appcovid19/models/country.dart';
import 'package:appcovid19/models/detailCountry.dart';
import 'package:appcovid19/models/responseApi.dart';
import 'package:appcovid19/repositories/apiHttp.dart';
import 'dart:convert' as convert;

class CountryRepository{

  Future<List<Country>> getListAll()async{
    List<Country> listResult = List();
    ApiHttp apiHttp = ApiHttp(endPoint: ConfigApp.getEndPointCovid19());
    ResponseApi response = await apiHttp.api(ApiMethod.GET, "/countries");
    if(response.codeResponse == 200){
      List bodyRequest = convert.jsonDecode(response.contentResponse);
      bodyRequest.forEach((item) {
        listResult.add(Country(
          Name: item['Country'].toString(),
          Slug: item['Slug'].toString(),
          ISO2: item['ISO2'].toString()
        ));
      });
    }
    return listResult;
  }

  Future<List<Country>> getListWithStatus()async{
    List<Country> listResult = List();
    ApiHttp apiHttp = ApiHttp(endPoint: ConfigApp.getEndPointCovid19());
    ResponseApi response = await apiHttp.api(ApiMethod.GET, "/summary");
    if(response.codeResponse == 200){
      var dataObj = convert.jsonDecode(response.contentResponse);
      List bodyRequest = dataObj['Countries'];
      bodyRequest.forEach((item) {
        listResult.add(Country(
          Name: item['Country'].toString(),
          Slug: item['Slug'].toString(),
          Code: item['CountryCode'].toString(),
          ISO2: "",
          NewConfirmed: int.parse(item['NewConfirmed'].toString()),
          NewDeaths: int.parse(item['NewDeaths'].toString()),
          NewRecovered: int.parse(item['NewRecovered'].toString()),
          TotalConfirmed: int.parse(item['TotalConfirmed'].toString()),
          TotalDeaths: int.parse(item['TotalDeaths'].toString()),
          TotalRecovered: int.parse(item['TotalRecovered'].toString())
        ));
      });
    }
    return listResult;
  }

  Future<List<DetailCountry>> getDetailCountry(String slug)async{
    List<DetailCountry> listResult = List();
    ApiHttp apiHttp = ApiHttp(endPoint: ConfigApp.getEndPointCovid19());
    ResponseApi response = await apiHttp.api(ApiMethod.GET, "/total/dayone/country/$slug");
    if(response.codeResponse == 200){
      List bodyRequest = convert.jsonDecode(response.contentResponse);
      bodyRequest.forEach((item) {
        listResult.add(DetailCountry(
          Confirmed: int.parse(item['Confirmed'].toString()),
          Deaths: int.parse(item['Deaths'].toString()),
          Recovered: int.parse(item['Recovered'].toString()),
          Date: item['Date'].toString()
        ));
      });
    }
    return listResult;
  }

}